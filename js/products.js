$(document).ready(function () {
    var page = 1;
    var current_page = 1;
    var total_page = 0;
    var is_ajax_fire = 0;

    manageData();
    getCategoriesData();

    function manageData() {
        $.ajax({
            dataType: 'json',
            url: url + 'api/products/get.php',
            data: {
                page: page
            }
        }).done(function (data) {
            total_page = Math.ceil(data.total / 10);
            current_page = page;
            $('#pagination').twbsPagination({
                totalPages: total_page,
                visiblePages: current_page,
                onPageClick: function (event, pageL) {
                    page = pageL;
                    if (is_ajax_fire != 0) {
                        getPageData();
                    }
                }
            });
            manageRow(data.data);
            is_ajax_fire = 1;
        });
    }

    /* Upload images */
    $("#upload").on("click", function(){
        var formData = new FormData($("#uploadimage")[0]);
        var route = "api/images/upload.php";
        $.ajax({
            url: route,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                toastr.success('Imagen subida con éxito', 'Operación finalizada', {
                    timeOut: 5000
                });
            }
        });
    });

    /* Get products */
    function getPageData() {
        $.ajax({
            dataType: 'json',
            url: url + 'api/products/get.php',
            data: {
                page: page
            }
        }).done(function (data) {
            manageRow(data.data);
        });
    }

    /* Show products */
    function manageRow(data) {
        var rows = '';
        $.each(data, function (key, value) {
            rows = rows + '<tr>';
            rows = rows + '<td>' + value.title + '</td>';
            rows = rows + '<td>' + value.description + '</td>';
            rows = rows + '<td>' + value.categoria + '</td>';
            rows = rows + '<td data-id="' + value.id + '">';
            rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Editar</button> ';
            rows = rows + '<button class="btn btn-danger remove-item">Eliminar</button>';
            rows = rows + '</td>';
            rows = rows + '</tr>';
        });
        $("tbody").html(rows);
    }

    /* Put data on create and edit selects */
    function getCategoriesData() {
        $.ajax({
            dataType: 'json',
            url: url + 'api/categories/get.php',
            data: {
                page: page
            }
        }).done(function (data) {
            $("#categories, #categories-edit").append('<option value="" default>Seleccione una categoría</option>');
            $.each(data.data, function(index, obj){
                $("#categories, #categories-edit").append('<option value="'+obj.id+'">'+obj.title+'</option>');
	        });
        });
    }

    /* New product */
    $("#createProduct").click(function(){
        var form_action = $("#create-product").find("form").attr("action");
        var title = $("#create-product").find("input[name='title']").val();
        var description = $("#create-product").find("textarea[name='description']").val();
        var categoria = $("#create-product").find("select[name='categoria']").val();
        if (title != '' && description != '' && categoria != '') {
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url + form_action,
                data: {
                    title: title,
                    description: description,
                    categoria: categoria
                }
            }).done(function (data) {
                $("#create-product").find("input[name='title']").val('');
                $("#create-product").find("textarea[name='description']").val('');
                getPageData();
                $(".modal").modal('hide');
                toastr.success('Producto agregado exitosamente', 'Operación finalizada', {
                    timeOut: 5000
                });
            });
        } else {
            alert('Ingrese campos obligatorios.');
        }
    });

    /* Delete product */
    $("body").on("click", ".remove-item", function () {
        var id = $(this).parent("td").data('id');
        var c_obj = $(this).parents("tr");
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url + 'api/products/delete.php',
            data: {
                id: id
            }
        }).done(function (data) {
            c_obj.remove();
            toastr.success('El producto fue eliminado exitosamente.', 'Operación finalizada', {
                timeOut: 5000
            });
            getPageData();
        });
    });

    /* Show edit product modal*/
    $("body").on("click", ".edit-item", function () {
        var id = $(this).parent("td").data('id');
        var title = $(this).parent("td").prev("td").prev("td").text();
        var description = $(this).parent("td").prev("td").prev("td").text();
        var categoria = $(this).parent("td").prev("td").text();
        $("#edit-item").find("input[name='title']").val(title);
        $("#edit-item").find("textarea[name='description']").val(description);
        $('#paises > option[value="3"]').attr('selected', 'selected');
        $('#categories-edit option:contains('+categoria+')').attr('selected', 'selected');
        $("#edit-item").find(".edit-id").val(id);
        
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url + 'api/images/get.php',
            data: {
                id:id
            }
        }).done(function (data) {
           var item = '';
           images = data.data;
            $.each(images, function (key, value) {
                item += '<li class="image">';
                item += '<a href="'+url+value.imagen+'"><img src="'+url+value.imagen+'"></a>';
                item += '</li>';
            });
            $("ul.prod-images li").remove();
            $("ul.prod-images").append(item);
        });
    });

    /* Edit product */
    $(".crud-submit-edit").click(function (e) {
        e.preventDefault();
        var form_action = $("#edit-item").find("form").attr("action");
        var title = $("#edit-item").find("input[name='title']").val();
        var description = $("#edit-item").find("textarea[name='description']").val();
        var categoria = $("#edit-item").find("select[name='categoria']").val();
        var id = $("#edit-item").find(".edit-id").val();
        if (title != '' && description != '') {
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url + form_action,
                data: {
                    title: title,
                    description: description,
                    id: id,
                    categoria: categoria
                }
            }).done(function (data) {
                getPageData();
                $(".modal").modal('hide');
                toastr.success('El producto fue editado exitosamente.', 'Operación finalizada', {
                    timeOut: 5000
                });
            });
        } else {
            alert('Por favor complete todos los campos.')
        }
    });
});