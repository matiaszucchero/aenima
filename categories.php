<!DOCTYPE html>
<html lang="es">

<head>
	<title>Categorias</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<link rel="stylesheet" type="text/css" href="./css/main.css">
</head>

<body>
	<nav class="navbar navbar-default" role="navigation">
		<ul class="nav navbar-nav navbar-header">
			<li><a href="#">Categorias</a></li>
			<li><a href="index.php">Productos</a></li>
		</ul>
		<button type="button" class="btn btn-success navbar-btn pull-right" data-toggle="modal" data-target="#create-category">Añadir categoria</button>
	</nav>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">Categorias</div>
			<div class="panel-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th width="200px">Accion</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<ul id="pagination" class="pagination-sm"></ul>
			</div>
		</div>
		<!-- Crear Categoria -->
		<div class="modal fade" id="create-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title" id="myModalLabel">Nueva Categoria</h4>
					</div>
					<div class="modal-body">
						<form data-toggle="validator" action="api/categories/create.php" method="POST" enctype="multipart/form-data">
							<div class="form-group">
								<label class="control-label" for="title">Nombre:</label>
								<input type="text" name="title" class="form-control" data-error="Please enter title." required />
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn crud-submit btn-success">Guardar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Editar Categoria  -->
		<div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title" id="myModalLabel">Editar Categoria</h4>
					</div>
					<div class="modal-body">
						<form data-toggle="validator" action="api/categories/update.php" method="put">
							<input type="hidden" name="id" class="edit-id">
							<div class="form-group">
								<label class="control-label" for="title">Nombre:</label>
								<input type="text" name="title" class="form-control" data-error="Please enter title." required />
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success crud-submit-edit">Guardar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script type="text/javascript" src="./js/config.js"></script>
	<script type="text/javascript" src="./js/categories.js"></script>
</body>

</html>