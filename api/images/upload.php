<?php
  require '../db_config.php';
  if (isset($_FILES["file"])) {
    $file = $_FILES["file"];
    $name = $file["name"];
    $type = $file["type"];
    $temp_route = $file["tmp_name"];
    $size = $file["size"];
    $dim = getimagesize($temp_route);
    $width = $dim[0];
    $height = $dim[1];
    $dir = "imgs/";

    if ($type != 'image/jpg' && $type != 'image/jpeg' && $type != 'image/png' && $type != 'image/gif') {
      echo "Error, el archivo no es una imagen"; 
    }
    else if ($size > 1024*1024) {
      echo "Error, el tamaño máximo permitido es un 1MB";
    }
    else if ($width > 500 || $height > 500) {
        echo "El alto y ancho máximo permitido es 500px";
    }
    else if($width < 60 || $height < 60) {
        echo "El alto y ancho máximo permitido es 60px";
    }
    else {
        $src = $dir.$name;
        move_uploaded_file($temp_route, $src);
        $sql = "INSERT INTO imagenes (id_prod, imagen) VALUES ('".$_POST['id']."','".$src."')";
        $result = $MySQLiconn->query($sql);
        $sql = "SELECT * FROM imagenes  WHERE id_prod=".$_POST['id']; 
        $result = $MySQLiconn->query($sql);
        $data = $result->fetch_assoc();
        echo json_encode($data);
    }
}
?>