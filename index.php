<!DOCTYPE html>
<html lang="es">

<head>
	<title>Productos</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
	<link rel="stylesheet" href="./css/main.css">
</head>

<body>
	<nav class="navbar navbar-default" role="navigation">
		<ul class="nav navbar-nav navbar-header">
			<li><a href="categories.php">Categorias</a></li>
			<li><a href="#">Productos</a></li>
		</ul>
		<button type="button" class="btn btn-success navbar-btn pull-right" data-toggle="modal" data-target="#create-product">Añadir producto</button>
	</nav>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">Productos</div>
			<div class="panel-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripcion</th>
							<th>Categoria</th>
							<th width="200px">Accion</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<ul id="pagination" class="pagination-sm"></ul>
			</div>
		</div>
	</div>
	<!-- Create product -->
	<div class="modal fade" id="create-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title" id="myModalLabel">Añadir producto</h4>
				</div>
				<div class="modal-body">
					<form data-toggle="validator" action="api/products/create.php" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label class="control-label" for="title">Nombre:</label>
							<input type="text" name="title" class="form-control" data-error="Ingrese el nombre del producto." required />
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label class="control-label" for="title">Descripcion:</label>
							<textarea name="description" class="form-control" data-error="Ingrese la descripción del producto." required></textarea>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label class="control-label" for="title">Categoria:</label>
							<select id="categories" name="categoria" class="form-control" data-error="Seleccione una categoría" required>
							</select>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<button type="button" id="createProduct" class="btn btn-success">Guardar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Edit product  -->
	<div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title" id="myModalLabel">Editar Producto</h4>
				</div>
				<div class="modal-body">
					<form data-toggle="validator" action="api/products/update.php" method="put">
						<input type="hidden" name="id" class="edit-id">
						<div class="form-group">
							<label class="control-label" for="title">Nombre:</label>
							<input type="text" name="title" class="form-control" data-error="Please enter title." required />
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label class="control-label" for="title">Descripcion:</label>
							<textarea name="description" class="form-control" data-error="Please enter description." required></textarea>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label class="control-label" for="title">Categoria:</label>
							<select id="categories-edit" name="categoria" class="form-control" data-error="Seleccione una categoría" required>
							</select>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-success crud-submit-edit">Guardar</button>
						</div>
					</form>

					<div class="form-groupr">
						<p>Imágenes</p>
						<ul class="prod-images"></ul>
					</div>

					<form id="uploadimage" method="post" enctype="multipart/form-data">
						<p class="text pull-left">Subir imagen</p>
						<input class="form-control" type="file" name="file" id="file" required />
						<input type="hidden" name="id" class="edit-id">
						<button id="upload" class="btn btn-info" type="button" class="submit">Subir</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script type="text/javascript" src="./js/config.js"></script>
	<script type="text/javascript" src="./js/products.js"></script>
</body>

</html>